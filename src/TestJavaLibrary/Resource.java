package TestJavaLibrary;

import java.util.HashMap;
import java.util.Map;

public class Resource {
    private Map<String,String> testMap;
    //Initialize resource with existing test methods. acts as database
    public Resource( Map<String,String> testMap){
        this.testMap=testMap;
    }
    //get all the test results on the map
    public Map<String, String> getTestMap() {
        return testMap;
    }
    //add new test method to the map
    public void save(String testMethodName,String status){
        testMap.put(testMethodName,status);
    }
}
