package TestJavaLibrary;

import java.io.Serializable;

public class DTO implements Serializable {

    private final String testMethodName;
    private final String status;
    public DTO(String testMethodName,String status){
        this.testMethodName=testMethodName;
        this.status=status;
    }
    public String getTestMethodName(){
        return testMethodName;
    }

    public String getStatus() {
        return status;
    }

}
